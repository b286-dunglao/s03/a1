from django.db import models

# Create your models here.
# - item_name - CharField and max_length = 50
# - category - CharField and max_length = 50
# - status - CharField, max_length = 50 and default value of pending
# - date_created DateTimeField

class GroceryItem(models.Model):
	item_name = models.CharField(max_length=50)
	category = models.CharField(max_length=50)
	status = models.CharField(max_length=50, default="pending")
	date_created = models.DateTimeField('date created')


	# from django_practice.models import GroceryItem;
	# from django.utils import timezone;

	# groceryitem = GroceryItem(item_name="Lays", category="Chips", date_created=timezone.now())
