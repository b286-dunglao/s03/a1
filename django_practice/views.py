from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse
from .models import GroceryItem
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = { 
		'groceryitem_list' : groceryitem_list,
		"user": request.user
	}
	return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
	groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	return render(request, "django_practice/groceryitem.html", groceryitem)

def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered": is_user_registered
	}

	for indiv_user in users:
	    if indiv_user.username == "sernserndunglao":
	        is_user_registered = True
	        break

	if is_user_registered == False:
	    user = User()
	    user.username = "sernserndunglao"
	    user.first_name = "sernsern"
	    user.last_name = "dunglao"
	    user.email = "sernsern.dunglao@mail.com"
	    # The set_password method is used to ensure that the password is hashed using Django's authentication framework
	    user.set_password("sernsern")
	    user.is_staff = False
	    user.is_active = True
	    user.save()
	    context = {
	        "first_name": user.first_name,
	        "last_name": user.last_name
	    }

	return render(request, "django_practice/register.html", context)


def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="sernserndunglao", password="sernsern")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='sernserndunglao')
        authenticated_user.set_password("sernsern1234")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "django_practice/change_password.html", context)

def login_view(request):
    username = "sernserndunglao"
    password = "sernsern1234"
    user = authenticate(username=username, password=password)
    context = {
        "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        # Saves the user’s ID in the session using Django's session framework
        login(request, user)
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")